﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testIngeneoApi.DTOs
{
    public class TipoDescuentoOtorgadoDTO
    {
        public int IdTipoDescuento { get; set; }
        public string TipoDescuento { get; set; }
        public short Descuento { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
