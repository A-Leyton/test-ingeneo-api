﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testIngeneoApi.DTOs
{
    public class TipoLogisticaDTO
    {
        public int IdTipoLogistica { get; set; }
        public string TipoLogistica1 { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
