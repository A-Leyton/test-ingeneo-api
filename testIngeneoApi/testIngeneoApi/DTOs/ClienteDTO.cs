﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testIngeneoApi.DTOs
{
    public class ClienteDTO
    {
        public int IdCliente { get; set; }
        public string Documento { get; set; }
        public string NombreCliente { get; set; }
        public string CorreoElectronico { get; set; }
        public string DireccionDomicilio { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public DateTime FechaRegistro { get; set; }
    }
}
