﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using testIngeneoApi.Models;

namespace testIngeneoApi.DTOs
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Cliente, ClienteDTO>();
                cfg.CreateMap<LogisticaVehiculo, LogisticaTerrestreDTO>();
                cfg.CreateMap<LogisticaMaritima, LogisticaMaritimaDTO>();
                cfg.CreateMap<TipoDescuentoOtorgado, TipoDescuentoOtorgadoDTO>();
                cfg.CreateMap<TipoLogistica, TipoLogisticaDTO>();
            });
            var mapper = config.CreateMapper();          
        }
    }
}
