﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testIngeneoApi.DTOs
{
    public class LogisticaMaritimaDTO
    {
        public int Id { get; set; }
        public int IdCliente { get; set; }
        public int IdTipoLogistica { get; set; }
        public string TipoProducto { get; set; }
        public int CantidadProducto { get; set; }
        public DateTime FechaRegisto { get; set; }
        public DateTime FechaEntrega { get; set; }
        public string BodegaEntrega { get; set; }
        public decimal PrecioEnvio { get; set; }
        public int IdTipoDescuento { get; set; }
        public string NumeroFlota { get; set; }
        public string NumeroGuia { get; set; }
    }
}
