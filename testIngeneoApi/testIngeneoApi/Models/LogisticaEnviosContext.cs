﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace testIngeneoApi.Models
{
    public partial class LogisticaEnviosContext : DbContext
    {
        public LogisticaEnviosContext()
        {
        }

        public LogisticaEnviosContext(DbContextOptions<LogisticaEnviosContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cliente> Clientes { get; set; }
        public virtual DbSet<LogisticaMaritima> LogisticaMaritimas { get; set; }
        public virtual DbSet<LogisticaVehiculo> LogisticaVehiculos { get; set; }
        public virtual DbSet<TipoDescuentoOtorgado> TipoDescuentoOtorgados { get; set; }
        public virtual DbSet<TipoLogistica> TipoLogisticas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Modern_Spanish_CI_AS");

            modelBuilder.Entity<Cliente>(entity =>
            {
                entity.HasKey(e => e.IdCliente)
                    .HasName("PK__Cliente__E005FBFF5A29DF8B");

                entity.ToTable("Cliente");

                entity.Property(e => e.IdCliente).HasColumnName("ID_Cliente");

                entity.Property(e => e.Celular)
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.CorreoElectronico)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.DireccionDomicilio)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Documento)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.FechaRegistro).HasColumnType("datetime");

                entity.Property(e => e.NombreCliente)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LogisticaMaritima>(entity =>
            {
                entity.ToTable("LogisticaMaritima");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BodegaEntrega)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaEntrega).HasColumnType("datetime");

                entity.Property(e => e.FechaRegisto).HasColumnType("datetime");

                entity.Property(e => e.IdCliente).HasColumnName("ID_Cliente");

                entity.Property(e => e.IdTipoDescuento).HasColumnName("ID_TipoDescuento");

                entity.Property(e => e.IdTipoLogistica).HasColumnName("ID_TipoLogistica");

                entity.Property(e => e.NumeroFlota)
                    .IsRequired()
                    .HasMaxLength(7);

                entity.Property(e => e.NumeroGuia)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.PrecioEnvio).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TipoProducto)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdClienteNavigation)
                    .WithMany(p => p.LogisticaMaritimas)
                    .HasForeignKey(d => d.IdCliente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Logistica__ID_Cl__2F10007B");

                entity.HasOne(d => d.IdTipoDescuentoNavigation)
                    .WithMany(p => p.LogisticaMaritimas)
                    .HasForeignKey(d => d.IdTipoDescuento)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Logistica__ID_Ti__30F848ED");

                entity.HasOne(d => d.IdTipoLogisticaNavigation)
                    .WithMany(p => p.LogisticaMaritimas)
                    .HasForeignKey(d => d.IdTipoLogistica)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Logistica__ID_Ti__300424B4");
            });

            modelBuilder.Entity<LogisticaVehiculo>(entity =>
            {
                entity.ToTable("LogisticaVehiculo");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.BodegaEntrega)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaEntrega).HasColumnType("datetime");

                entity.Property(e => e.FechaRegisto).HasColumnType("datetime");

                entity.Property(e => e.IdCliente).HasColumnName("ID_Cliente");

                entity.Property(e => e.IdTipoDescuento).HasColumnName("ID_TipoDescuento");

                entity.Property(e => e.IdTipoLogistica).HasColumnName("ID_TipoLogistica");

                entity.Property(e => e.NumeroGuia)
                    .IsRequired()
                    .HasMaxLength(10);

                entity.Property(e => e.PlacaVehiculo)
                    .IsRequired()
                    .HasMaxLength(6);

                entity.Property(e => e.PrecioEnvio).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.TipoProducto)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdClienteNavigation)
                    .WithMany(p => p.LogisticaVehiculos)
                    .HasForeignKey(d => d.IdCliente)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Logistica__ID_Cl__2A4B4B5E");

                entity.HasOne(d => d.IdTipoDescuentoNavigation)
                    .WithMany(p => p.LogisticaVehiculos)
                    .HasForeignKey(d => d.IdTipoDescuento)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Logistica__ID_Ti__2C3393D0");

                entity.HasOne(d => d.IdTipoLogisticaNavigation)
                    .WithMany(p => p.LogisticaVehiculos)
                    .HasForeignKey(d => d.IdTipoLogistica)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Logistica__ID_Ti__2B3F6F97");
            });

            modelBuilder.Entity<TipoDescuentoOtorgado>(entity =>
            {
                entity.HasKey(e => e.IdTipoDescuento)
                    .HasName("PK__TipoDesc__8BFA279EDFEAC02B");

                entity.ToTable("TipoDescuentoOtorgado");

                entity.Property(e => e.IdTipoDescuento).HasColumnName("ID_TipoDescuento");

                entity.Property(e => e.FechaRegistro).HasColumnType("datetime");

                entity.Property(e => e.TipoDescuento)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TipoLogistica>(entity =>
            {
                entity.HasKey(e => e.IdTipoLogistica)
                    .HasName("PK__TipoLogi__2040586395374E74");

                entity.ToTable("TipoLogistica");

                entity.Property(e => e.IdTipoLogistica).HasColumnName("ID_TipoLogistica");

                entity.Property(e => e.FechaRegistro).HasColumnType("datetime");

                entity.Property(e => e.TipoLogistica1)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("TipoLogistica");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
