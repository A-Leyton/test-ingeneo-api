﻿using System;
using System.Collections.Generic;

#nullable disable

namespace testIngeneoApi.Models
{
    public partial class LogisticaVehiculo
    {
        public int Id { get; set; }
        public int IdCliente { get; set; }
        public int IdTipoLogistica { get; set; }
        public string TipoProducto { get; set; }
        public int CantidadProducto { get; set; }
        public DateTime FechaRegisto { get; set; }
        public DateTime FechaEntrega { get; set; }
        public string BodegaEntrega { get; set; }
        public decimal PrecioEnvio { get; set; }
        public int IdTipoDescuento { get; set; }
        public string PlacaVehiculo { get; set; }
        public string NumeroGuia { get; set; }

        public virtual Cliente IdClienteNavigation { get; set; }
        public virtual TipoDescuentoOtorgado IdTipoDescuentoNavigation { get; set; }
        public virtual TipoLogistica IdTipoLogisticaNavigation { get; set; }
    }
}
