﻿using System;
using System.Collections.Generic;

#nullable disable

namespace testIngeneoApi.Models
{
    public partial class TipoDescuentoOtorgado
    {
        public TipoDescuentoOtorgado()
        {
            LogisticaMaritimas = new HashSet<LogisticaMaritima>();
            LogisticaVehiculos = new HashSet<LogisticaVehiculo>();
        }

        public int IdTipoDescuento { get; set; }
        public string TipoDescuento { get; set; }
        public short Descuento { get; set; }
        public DateTime FechaRegistro { get; set; }

        public virtual ICollection<LogisticaMaritima> LogisticaMaritimas { get; set; }
        public virtual ICollection<LogisticaVehiculo> LogisticaVehiculos { get; set; }
    }
}
