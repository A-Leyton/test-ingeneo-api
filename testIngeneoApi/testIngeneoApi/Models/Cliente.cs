﻿using System;
using System.Collections.Generic;

#nullable disable

namespace testIngeneoApi.Models
{
    public partial class Cliente
    {
        public Cliente()
        {
            LogisticaMaritimas = new HashSet<LogisticaMaritima>();
            LogisticaVehiculos = new HashSet<LogisticaVehiculo>();
        }

        public int IdCliente { get; set; }
        public string Documento { get; set; }
        public string NombreCliente { get; set; }
        public string CorreoElectronico { get; set; }
        public string DireccionDomicilio { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public DateTime FechaRegistro { get; set; }

        public virtual ICollection<LogisticaMaritima> LogisticaMaritimas { get; set; }
        public virtual ICollection<LogisticaVehiculo> LogisticaVehiculos { get; set; }
    }
}
