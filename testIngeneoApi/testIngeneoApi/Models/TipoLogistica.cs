﻿using System;
using System.Collections.Generic;

#nullable disable

namespace testIngeneoApi.Models
{
    public partial class TipoLogistica
    {
        public TipoLogistica()
        {
            LogisticaMaritimas = new HashSet<LogisticaMaritima>();
            LogisticaVehiculos = new HashSet<LogisticaVehiculo>();
        }

        public int IdTipoLogistica { get; set; }
        public string TipoLogistica1 { get; set; }
        public DateTime FechaRegistro { get; set; }

        public virtual ICollection<LogisticaMaritima> LogisticaMaritimas { get; set; }
        public virtual ICollection<LogisticaVehiculo> LogisticaVehiculos { get; set; }
    }
}
