﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using testIngeneoApi.Models;

namespace testIngeneoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogisticaMaritimaController : ControllerBase
    {
        private readonly LogisticaEnviosContext _context;

        public LogisticaMaritimaController(LogisticaEnviosContext context)
        {
            _context = context;
        }

        // GET: api/LogisticaMaritima
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LogisticaMaritima>>> GetLogisticaMaritimas()
        {
            return await _context.LogisticaMaritimas.ToListAsync();
        }

        // GET: api/LogisticaMaritima/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LogisticaMaritima>> GetLogisticaMaritima(int id)
        {
            var logisticaMaritima = await _context.LogisticaMaritimas.FindAsync(id);

            if (logisticaMaritima == null)
            {
                return NotFound();
            }

            return logisticaMaritima;
        }

        // PUT: api/LogisticaMaritima/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLogisticaMaritima(int id, LogisticaMaritima logisticaMaritima)
        {
            if (id != logisticaMaritima.Id)
            {
                return BadRequest();
            }

            _context.Entry(logisticaMaritima).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LogisticaMaritimaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LogisticaMaritima
        [HttpPost]
        public async Task<ActionResult<LogisticaMaritima>> PostLogisticaMaritima(LogisticaMaritima logisticaMaritima)
        {
            _context.LogisticaMaritimas.Add(logisticaMaritima);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLogisticaMaritima", new { id = logisticaMaritima.Id }, logisticaMaritima);
        }

        // DELETE: api/LogisticaMaritima/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLogisticaMaritima(int id)
        {
            var logisticaMaritima = await _context.LogisticaMaritimas.FindAsync(id);
            if (logisticaMaritima == null)
            {
                return NotFound();
            }

            _context.LogisticaMaritimas.Remove(logisticaMaritima);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool LogisticaMaritimaExists(int id)
        {
            return _context.LogisticaMaritimas.Any(e => e.Id == id);
        }
    }
}
