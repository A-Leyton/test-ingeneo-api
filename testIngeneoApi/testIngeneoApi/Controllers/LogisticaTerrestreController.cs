﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using testIngeneoApi.Models;

namespace testIngeneoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogisticaTerrestreController : ControllerBase
    {
        private readonly LogisticaEnviosContext _context;

        public LogisticaTerrestreController(LogisticaEnviosContext context)
        {
            _context = context;
        }

        // GET: api/LogisticaTerrestre
        [HttpGet]
        public async Task<ActionResult<IEnumerable<LogisticaVehiculo>>> GetLogisticaVehiculos()
        {
            return await _context.LogisticaVehiculos.ToListAsync();
        }

        // GET: api/LogisticaTerrestre/5
        [HttpGet("{id}")]
        public async Task<ActionResult<LogisticaVehiculo>> GetLogisticaVehiculo(int id)
        {
            var logisticaVehiculo = await _context.LogisticaVehiculos.FindAsync(id);

            if (logisticaVehiculo == null)
            {
                return NotFound();
            }

            return logisticaVehiculo;
        }

        // PUT: api/LogisticaTerrestre/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLogisticaVehiculo(int id, LogisticaVehiculo logisticaVehiculo)
        {
            if (id != logisticaVehiculo.Id)
            {
                return BadRequest();
            }

            _context.Entry(logisticaVehiculo).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LogisticaVehiculoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LogisticaTerrestre
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<LogisticaVehiculo>> PostLogisticaVehiculo(LogisticaVehiculo logisticaVehiculo)
        {
            _context.LogisticaVehiculos.Add(logisticaVehiculo);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLogisticaVehiculo", new { id = logisticaVehiculo.Id }, logisticaVehiculo);
        }

        // DELETE: api/LogisticaTerrestre/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLogisticaVehiculo(int id)
        {
            var logisticaVehiculo = await _context.LogisticaVehiculos.FindAsync(id);
            if (logisticaVehiculo == null)
            {
                return NotFound();
            }

            _context.LogisticaVehiculos.Remove(logisticaVehiculo);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool LogisticaVehiculoExists(int id)
        {
            return _context.LogisticaVehiculos.Any(e => e.Id == id);
        }
    }
}
