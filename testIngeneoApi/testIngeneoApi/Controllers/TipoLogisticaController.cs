﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using testIngeneoApi.Models;

namespace testIngeneoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoLogisticaController : ControllerBase
    {
        private readonly LogisticaEnviosContext _context;

        public TipoLogisticaController(LogisticaEnviosContext context)
        {
            _context = context;
        }

        // GET: api/TipoLogistica
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TipoLogistica>>> GetTipoLogisticas()
        {
            return await _context.TipoLogisticas.ToListAsync();
        }

        // GET: api/TipoLogistica/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TipoLogistica>> GetTipoLogistica(int id)
        {
            var tipoLogistica = await _context.TipoLogisticas.FindAsync(id);

            if (tipoLogistica == null)
            {
                return NotFound();
            }

            return tipoLogistica;
        }

        // PUT: api/TipoLogistica/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTipoLogistica(int id, TipoLogistica tipoLogistica)
        {
            if (id != tipoLogistica.IdTipoLogistica)
            {
                return BadRequest();
            }

            _context.Entry(tipoLogistica).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoLogisticaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TipoLogistica
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<TipoLogistica>> PostTipoLogistica(TipoLogistica tipoLogistica)
        {
            _context.TipoLogisticas.Add(tipoLogistica);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTipoLogistica", new { id = tipoLogistica.IdTipoLogistica }, tipoLogistica);
        }

        // DELETE: api/TipoLogistica/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTipoLogistica(int id)
        {
            var tipoLogistica = await _context.TipoLogisticas.FindAsync(id);
            if (tipoLogistica == null)
            {
                return NotFound();
            }

            _context.TipoLogisticas.Remove(tipoLogistica);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool TipoLogisticaExists(int id)
        {
            return _context.TipoLogisticas.Any(e => e.IdTipoLogistica == id);
        }
    }
}
