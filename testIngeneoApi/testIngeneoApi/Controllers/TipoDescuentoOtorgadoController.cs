﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using testIngeneoApi.Models;

namespace testIngeneoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoDescuentoOtorgadoController : ControllerBase
    {
        private readonly LogisticaEnviosContext _context;

        public TipoDescuentoOtorgadoController(LogisticaEnviosContext context)
        {
            _context = context;
        }

        // GET: api/TipoDescuentoOtorgado
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TipoDescuentoOtorgado>>> GetTipoDescuentoOtorgados()
        {
            return await _context.TipoDescuentoOtorgados.ToListAsync();
        }

        // GET: api/TipoDescuentoOtorgado/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TipoDescuentoOtorgado>> GetTipoDescuentoOtorgado(int id)
        {
            var tipoDescuentoOtorgado = await _context.TipoDescuentoOtorgados.FindAsync(id);

            if (tipoDescuentoOtorgado == null)
            {
                return NotFound();
            }

            return tipoDescuentoOtorgado;
        }

        // PUT: api/TipoDescuentoOtorgado/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTipoDescuentoOtorgado(int id, TipoDescuentoOtorgado tipoDescuentoOtorgado)
        {
            if (id != tipoDescuentoOtorgado.IdTipoDescuento)
            {
                return BadRequest();
            }

            _context.Entry(tipoDescuentoOtorgado).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoDescuentoOtorgadoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TipoDescuentoOtorgado
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<TipoDescuentoOtorgado>> PostTipoDescuentoOtorgado(TipoDescuentoOtorgado tipoDescuentoOtorgado)
        {
            _context.TipoDescuentoOtorgados.Add(tipoDescuentoOtorgado);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTipoDescuentoOtorgado", new { id = tipoDescuentoOtorgado.IdTipoDescuento }, tipoDescuentoOtorgado);
        }

        // DELETE: api/TipoDescuentoOtorgado/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTipoDescuentoOtorgado(int id)
        {
            var tipoDescuentoOtorgado = await _context.TipoDescuentoOtorgados.FindAsync(id);
            if (tipoDescuentoOtorgado == null)
            {
                return NotFound();
            }

            _context.TipoDescuentoOtorgados.Remove(tipoDescuentoOtorgado);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool TipoDescuentoOtorgadoExists(int id)
        {
            return _context.TipoDescuentoOtorgados.Any(e => e.IdTipoDescuento == id);
        }
    }
}
